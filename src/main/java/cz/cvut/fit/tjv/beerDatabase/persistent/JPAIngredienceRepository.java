package cz.cvut.fit.tjv.beerDatabase.persistent;


import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;
@Repository
public interface JPAIngredienceRepository extends JpaRepository<Ingredience,Long> {
    public Ingredience findIngrediencesByName(String name);



}
