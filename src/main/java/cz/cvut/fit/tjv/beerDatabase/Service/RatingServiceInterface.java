package cz.cvut.fit.tjv.beerDatabase.Service;

import cz.cvut.fit.tjv.beerDatabase.domain.Rating;
import cz.cvut.fit.tjv.beerDatabase.persistent.JPARatingRepository;

import java.util.List;

public interface RatingServiceInterface {

    public Rating getRatingById(Long id);
    public List<Rating> getRatings();
    public Rating addRating(Rating rating);
    public Rating updateRating(Rating rating);
    public void deleteRating(Long id);

}
