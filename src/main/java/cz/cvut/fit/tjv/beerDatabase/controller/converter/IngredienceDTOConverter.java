package cz.cvut.fit.tjv.beerDatabase.controller.converter;

import cz.cvut.fit.tjv.beerDatabase.Service.BeerServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.IngredienceDTO;
import cz.cvut.fit.tjv.beerDatabase.domain.Beer;
import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import org.springframework.stereotype.Component;

@Component

public class IngredienceDTOConverter implements DTOConverter<IngredienceDTO, Ingredience>{
    private final BeerServiceInterface beerService;

    public IngredienceDTOConverter(BeerServiceInterface beerService){
        this.beerService = beerService;
    }
    @Override
    public IngredienceDTO toDTO(Ingredience entity) {
        return new IngredienceDTO(
                entity.getId(),
                entity.getName(),
                entity.getBeers().stream().map(Beer::getId).toList()
        );
    }

    @Override
    public Ingredience toEntity(IngredienceDTO dto) {
        return new Ingredience(
                dto.getId(),
                dto.getName(),
                dto.getBeerIds().stream().map(beerService::getBeerById).toList());
    }


}
