package cz.cvut.fit.tjv.beerDatabase.controller.converter;

import cz.cvut.fit.tjv.beerDatabase.Service.BeerServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.RatingDTO;
import cz.cvut.fit.tjv.beerDatabase.domain.Rating;
import org.springframework.stereotype.Component;

@Component

public class RatingDTOConverter implements DTOConverter<RatingDTO, Rating>{
    private final BeerServiceInterface beerService;
    public RatingDTOConverter(BeerServiceInterface beerService){
        this.beerService = beerService;
    }
    @Override
    public RatingDTO toDTO(Rating entity) {
        return new RatingDTO(
                entity.getId(),
                entity.getRating(),
                entity.getRatedBeer().getId()
        );
    }

    @Override
    public Rating toEntity(RatingDTO dto) {
        return new Rating(
                dto.getId(),
                dto.getRating(),
                beerService.getBeerById(dto.getBeerId())

        );
    }
}
