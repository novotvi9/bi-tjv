package cz.cvut.fit.tjv.beerDatabase.controller.converter;

public interface DTOConverter <DTO, ENTITY>{
    DTO toDTO(ENTITY e);
    ENTITY toEntity(DTO dto);
}
