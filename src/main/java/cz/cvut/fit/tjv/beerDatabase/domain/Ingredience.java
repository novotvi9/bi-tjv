package cz.cvut.fit.tjv.beerDatabase.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Entity
@Getter
@Setter
public class Ingredience {
    @Id
    @Column(name="id_ingredience")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @SequenceGenerator(name = "seq", sequenceName = "ingredience_id_seq", allocationSize = 1)
    Long id;
    @Column(name = "name_ingredience")
    String name;
    @ManyToMany(mappedBy = "ingrediences", cascade = CascadeType.ALL)
    List<Beer> beers;
    public Ingredience(Long id, String name, List<Beer> beers) {
        this.id = id;
        this.name = name;
        this.beers = beers;
    }
    public Ingredience() {
    }
    public Ingredience(Long id,String name){
        this.id = id;
        this.name = name;
    }

}
