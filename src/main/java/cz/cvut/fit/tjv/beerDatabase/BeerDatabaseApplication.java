package cz.cvut.fit.tjv.beerDatabase;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@OpenAPIDefinition
public class BeerDatabaseApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeerDatabaseApplication.class, args);
	}

}
