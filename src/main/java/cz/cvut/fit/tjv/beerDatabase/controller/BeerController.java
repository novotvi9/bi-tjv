package cz.cvut.fit.tjv.beerDatabase.controller;

import cz.cvut.fit.tjv.beerDatabase.Service.IngredienceServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.Service.BeerServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.Service.RatingServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.controller.converter.DTOConverter;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.BeerDTO;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.IngredienceDTO;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.RatingDTO;
import cz.cvut.fit.tjv.beerDatabase.domain.Beer;
import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import cz.cvut.fit.tjv.beerDatabase.domain.Rating;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
@RestController
@RequestMapping("/beer")
public class BeerController {
    private final BeerServiceInterface beerService;
    public final IngredienceServiceInterface ingredienceService;
    public final RatingServiceInterface ratingService;
    public final DTOConverter<BeerDTO, Beer> beerDTOConverter;
    public final DTOConverter<IngredienceDTO, Ingredience> ingredienceDTOConverter;
    public final DTOConverter<RatingDTO, Rating> ratingDTOConverter;
    public BeerController(BeerServiceInterface beerService, IngredienceServiceInterface ingredienceService,
                          RatingServiceInterface ratingService, DTOConverter<BeerDTO, Beer> beerDTOConverter,
                          DTOConverter<IngredienceDTO, Ingredience> ingredienceDTOConverter,
                          DTOConverter<RatingDTO, Rating> ratingDTOConverter){
        this.beerService = beerService;
        this.ingredienceService = ingredienceService;
        this.ratingService = ratingService;
        this.beerDTOConverter = beerDTOConverter;
        this.ingredienceDTOConverter = ingredienceDTOConverter;
        this.ratingDTOConverter = ratingDTOConverter;
    }

    @GetMapping
    public List<BeerDTO> getBeers(){
        return beerService.getBeers().stream().map(beerDTOConverter::toDTO).toList();

    }
    @GetMapping(path = "/{id}")
    public BeerDTO getBeer(@PathVariable("id") Long id){
        return beerDTOConverter.toDTO(beerService.getBeerById(id));
    }
    @GetMapping(path = "/{id}/ingrediences")
    public List<IngredienceDTO> getIngrediences(@PathVariable("id") Long id){
        return beerService.getBeerById(id).getIngrediences().stream().map(ingredienceDTOConverter::toDTO).toList();
    }
    @PostMapping
    public BeerDTO addBeer(@RequestBody BeerDTO beer){
        return beerDTOConverter.toDTO(beerService.addBeer(beerDTOConverter.toEntity(beer)));
    }
    @PutMapping(path = "/{id}")
    public BeerDTO updateBeer(@PathVariable("id") Long id, @RequestBody BeerDTO beer){
        Beer entity = beerDTOConverter.toEntity(beer);
        entity.setId(id);
        return beerDTOConverter.toDTO(beerService.updateBeer(entity));
    }
    @DeleteMapping(path = "/{id}")
    public void deleteBeer(@PathVariable("id") Long id){
        beerService.deleteBeer(id);
    }
    @PostMapping(path = "/{id}/ingrediences")
    public void addIngredience(@PathVariable("id") Long beerId, @RequestBody Long ingredienceId){
        Ingredience ingredience = ingredienceService.getIngredienceById(ingredienceId);
        beerService.addIngredience(beerId,ingredience);
    }
    @GetMapping(path = "/rating/{rating}")
    public List<BeerDTO> getBeersByRating(@PathVariable Long rating){
        return beerService.findBeersByReviewsGreaterThanEqual(rating).stream().map(beerDTOConverter::toDTO).toList();
    }


}
