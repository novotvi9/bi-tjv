package cz.cvut.fit.tjv.beerDatabase.Service;

import cz.cvut.fit.tjv.beerDatabase.domain.Beer;
import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import java.util.List;

public interface BeerServiceInterface {
    public Beer getBeerById(Long id)throws EntityNotFoundException;
    public List<Beer> getBeers();

    public Beer addBeer(Beer beer) throws EntityExistsException;

    public Beer updateBeer (Beer beer) throws IllegalArgumentException;

    public void deleteBeer(Long id) throws EntityNotFoundException;
    public void addIngredience(Long id, Ingredience ingredience);
    public List<Beer> findBeersByReviewsGreaterThanEqual(Long rating);


}
