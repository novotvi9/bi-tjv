package cz.cvut.fit.tjv.beerDatabase.Service;

import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import cz.cvut.fit.tjv.beerDatabase.persistent.JPAIngredienceRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class IngredienceService implements IngredienceServiceInterface {

    private JPAIngredienceRepository ingredienceRepository;
    public IngredienceService(JPAIngredienceRepository ingredienceRepository){this.ingredienceRepository=ingredienceRepository;}
    @Override
    public Ingredience getIngredienceById(Long id)throws EntityNotFoundException {
        return ingredienceRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Beer with given id: " + id + " not found"));
    }

    @Override
    public List<Ingredience> getIngrediences() {
        return ingredienceRepository.findAll();
    }

    @Override
    public Ingredience addIngredience(Ingredience ingredience) throws EntityExistsException {
        if(ingredienceRepository.findIngrediencesByName(ingredience.getName())==null)
            return ingredienceRepository.save(ingredience);
        throw new EntityExistsException("Ingredience with given name"+ ingredience.getName()+" already exists");
    }

    @Override
    public Ingredience updateIngredience(Ingredience ingredience) throws IllegalArgumentException {
        if(ingredienceRepository.existsById(ingredience.getId())){
            ingredienceRepository.save(ingredience);
        }
        throw new IllegalArgumentException("Ingredience is not in the database");
    }

    @Override
    public void deleteIngredience(Long id) throws EntityNotFoundException {
        if(ingredienceRepository.existsById(id)){
            ingredienceRepository.deleteById(id);
        }
        else
            throw new EntityNotFoundException("Ingredience with given id: "+ id + " not found");

    }



}

