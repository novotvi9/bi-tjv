package cz.cvut.fit.tjv.beerDatabase.controller;

import cz.cvut.fit.tjv.beerDatabase.Service.BeerServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.Service.IngredienceServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.Service.RatingServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.controller.converter.DTOConverter;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.BeerDTO;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.IngredienceDTO;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.RatingDTO;
import cz.cvut.fit.tjv.beerDatabase.domain.Beer;
import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import cz.cvut.fit.tjv.beerDatabase.domain.Rating;
import org.springframework.web.bind.annotation.*;


import java.util.List;
@RestController
@RequestMapping("/rating")

public class RatingController {
    private final BeerServiceInterface beerService;
    public final IngredienceServiceInterface ingredienceService;
    public final RatingServiceInterface ratingService;
    public final DTOConverter<BeerDTO, Beer> beerDTOConverter;
    public final DTOConverter<IngredienceDTO, Ingredience> ingredienceDTOConverter;
    public final DTOConverter<RatingDTO, Rating> ratingDTOConverter;
    public RatingController(BeerServiceInterface beerService, IngredienceServiceInterface ingredienceService,
                            RatingServiceInterface ratingService, DTOConverter<BeerDTO, Beer> beerDTOConverter,
                            DTOConverter<IngredienceDTO, Ingredience> ingredienceDTOConverter,
                            DTOConverter<RatingDTO, Rating> ratingDTOConverter){
        this.beerService = beerService;
        this.ingredienceService = ingredienceService;
        this.ratingService = ratingService;
        this.beerDTOConverter = beerDTOConverter;
        this.ingredienceDTOConverter = ingredienceDTOConverter;
        this.ratingDTOConverter = ratingDTOConverter;
    }
    @GetMapping
    public List<RatingDTO> getRatings(){
        return ratingService.getRatings().stream().map(ratingDTOConverter::toDTO).toList();
    }
    @GetMapping(path = "/{id}")
    public RatingDTO getRating(@PathVariable("id") Long id){
        return ratingDTOConverter.toDTO(ratingService.getRatingById(id));
    }
    @GetMapping(path = "/{id}/beer")
    public BeerDTO getBeer(@PathVariable("id") Long id){
        return beerDTOConverter.toDTO(ratingService.getRatingById(id).getRatedBeer());
    }
    @PostMapping
    public RatingDTO addRating(@RequestBody RatingDTO rating){
        return ratingDTOConverter.toDTO(ratingService.addRating(ratingDTOConverter.toEntity(rating)));
    }
    @PutMapping
    public RatingDTO updateRating(@PathVariable("id") Long id, @RequestBody RatingDTO rating){
        Rating entity = ratingDTOConverter.toEntity(rating);
        entity.setId(id);
        return ratingDTOConverter.toDTO(ratingService.updateRating(entity));
    }
    @DeleteMapping
    public void deleteRating(@PathVariable("id") Long id){
        ratingService.deleteRating(id);
    }
}
