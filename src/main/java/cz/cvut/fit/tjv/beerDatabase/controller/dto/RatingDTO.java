package cz.cvut.fit.tjv.beerDatabase.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class RatingDTO {
    private final Long id;
    private final Long rating;
    private final Long beerId;
}
