package cz.cvut.fit.tjv.beerDatabase.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
@Entity
@Getter
@Setter
public class Rating {
    @Id
    @Column(name = "id_rating")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @SequenceGenerator(name = "seq", sequenceName = "rating_id_seq", allocationSize = 1)
    Long id;
    @Column(name = "rating")
    Long rating;
    @ManyToOne(targetEntity = Beer.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "rated_beer")
    Beer ratedBeer;
    public Rating(Long id, Long rating, Beer ratedBeer) {
        this.id = id;
        this.rating = rating;
        this.ratedBeer = ratedBeer;
    }
    public Rating() {
    }



}
