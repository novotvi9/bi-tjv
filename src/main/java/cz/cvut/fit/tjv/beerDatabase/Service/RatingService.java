package cz.cvut.fit.tjv.beerDatabase.Service;

import cz.cvut.fit.tjv.beerDatabase.domain.Rating;
import cz.cvut.fit.tjv.beerDatabase.persistent.JPABeerRepository;
import cz.cvut.fit.tjv.beerDatabase.persistent.JPARatingRepository;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class RatingService implements RatingServiceInterface {
    private JPARatingRepository ratingRepository;
    public RatingService(JPARatingRepository ratingRepository){this.ratingRepository=ratingRepository;}
    @Override
    public Rating getRatingById(Long id) throws EntityNotFoundException {
        return ratingRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Rating with given id not found"));

    }

    @Override
    public List<Rating> getRatings() {
        return ratingRepository.findAll();
    }

    @Override
    public Rating addRating(Rating rating) {
        return ratingRepository.save(rating);

    }

    @Override
    public Rating updateRating(Rating rating) {
        if( ratingRepository.existsById(rating.getId())){
            return ratingRepository.save(rating);
        }
        throw new IllegalArgumentException("rating is not in the database");
    }

    @Override
    public void deleteRating(Long id) {
        if(ratingRepository.existsById(id)){
            ratingRepository.deleteById(id);
        }
        else
            throw new EntityNotFoundException("Rating with given id: " + id + " not found");

    }


}
