package cz.cvut.fit.tjv.beerDatabase.persistent;

import cz.cvut.fit.tjv.beerDatabase.domain.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface JPARatingRepository extends JpaRepository<Rating,Long> {


}
