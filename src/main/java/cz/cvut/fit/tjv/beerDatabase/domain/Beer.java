package cz.cvut.fit.tjv.beerDatabase.domain;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;
@Entity
@Getter
@Setter

public class Beer {
    @Id
    @Column(name = "id_beer")
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "seq")
    @SequenceGenerator(name = "seq", sequenceName = "beer_id_seq", allocationSize = 1)
    Long id;
    @Column(name = "name_beer")
    @NotBlank
    String name;
    @ManyToMany(targetEntity = Ingredience.class, cascade = CascadeType.ALL)
    @JoinTable(
            name = "ingredience_in_beer",
            joinColumns = @JoinColumn(name = "id_ingredience"),
            inverseJoinColumns = @JoinColumn(name = "id_beer")
    )
    List<Ingredience> ingrediences= new ArrayList<>();
    @OneToMany(targetEntity = Rating.class, mappedBy = "ratedBeer", cascade = CascadeType.ALL)
    List<Rating> reviews = new ArrayList<>();
    public Beer(Long id, String name, List<Ingredience> ingrediences, List<Rating> reviews) {
        this.id = id;
        this.name = name;
        this.ingrediences = ingrediences;
        this.reviews = reviews;

    }
    public Beer(String name){
        this.name = name;
    }
    public Beer() {
    }


}
