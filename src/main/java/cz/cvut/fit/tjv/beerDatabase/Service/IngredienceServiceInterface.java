package cz.cvut.fit.tjv.beerDatabase.Service;

import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;

import java.util.List;
public interface IngredienceServiceInterface {
    public Ingredience getIngredienceById(Long id) throws EntityNotFoundException;
    public List<Ingredience> getIngrediences();
    public Ingredience addIngredience(Ingredience ingredience) throws EntityExistsException;
    public Ingredience updateIngredience(Ingredience ingredience) throws IllegalArgumentException;
    public void deleteIngredience(Long id) throws EntityNotFoundException;


}
