package cz.cvut.fit.tjv.beerDatabase.Service;

import cz.cvut.fit.tjv.beerDatabase.domain.Beer;
import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import cz.cvut.fit.tjv.beerDatabase.persistent.JPABeerRepository;
import cz.cvut.fit.tjv.beerDatabase.persistent.JPAIngredienceRepository;
import jakarta.persistence.EntityExistsException;
import jakarta.persistence.EntityNotFoundException;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public class BeerService implements BeerServiceInterface {
    private JPABeerRepository beerRepository;
    private JPAIngredienceRepository ingredienceRepository;
    public BeerService(JPABeerRepository beerRepository, JPAIngredienceRepository ingredienceRepository){

        this.beerRepository=beerRepository;
        this.ingredienceRepository=ingredienceRepository;
    }
    @Override
    public Beer getBeerById(Long id) throws EntityNotFoundException {
        return beerRepository.findById(id).orElseThrow(()->new EntityNotFoundException("Beer not found"));
    }

    @Override
    public List<Beer> getBeers() {
        return beerRepository.findAll();
    }

    @Override
    public Beer addBeer(Beer beer) throws EntityExistsException {
        if(beerRepository.findBeerByName(beer.getName()) == null)
            return beerRepository.save(beer);
        throw new EntityExistsException("Beer is already in database");
    }

    @Override
    public Beer updateBeer(Beer beer)throws IllegalArgumentException {
        if(beerRepository.existsById(beer.getId())){
            return beerRepository.save(beer);
        }
        throw new IllegalArgumentException("Beer is not in the database");
    }

    @Override
    public void deleteBeer(Long id) throws EntityNotFoundException {
        if(beerRepository.existsById(id)){
            beerRepository.deleteById(id);
        }
        else
            throw new EntityNotFoundException("Beer with id:" + id +" not found ");
    }

    @Override
    public void addIngredience(Long id, Ingredience ingredience) throws EntityNotFoundException {
        getBeerById(id).getIngrediences().add(ingredience);
    }

    @Override
    public List<Beer> findBeersByReviewsGreaterThanEqual(Long rating) {
        return beerRepository.findBeersByReviewsGreaterThanEqual(rating);
    }


}
