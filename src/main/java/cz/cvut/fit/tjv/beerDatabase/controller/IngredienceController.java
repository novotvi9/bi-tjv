package cz.cvut.fit.tjv.beerDatabase.controller;

import cz.cvut.fit.tjv.beerDatabase.Service.BeerServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.Service.IngredienceServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.Service.RatingServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.controller.converter.DTOConverter;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.BeerDTO;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.IngredienceDTO;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.RatingDTO;
import cz.cvut.fit.tjv.beerDatabase.domain.Beer;
import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import cz.cvut.fit.tjv.beerDatabase.domain.Rating;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/ingredience")
public class IngredienceController {
    private final BeerServiceInterface beerService;
    public final IngredienceServiceInterface ingredienceService;
    public final RatingServiceInterface ratingService;
    public final DTOConverter<BeerDTO, Beer> beerDTOConverter;
    public final DTOConverter<IngredienceDTO, Ingredience> ingredienceDTOConverter;
    public final DTOConverter<RatingDTO, Rating> ratingDTOConverter;
    public IngredienceController(BeerServiceInterface beerService, IngredienceServiceInterface ingredienceService,
                                 RatingServiceInterface ratingService, DTOConverter<BeerDTO, Beer> beerDTOConverter,
                                 DTOConverter<IngredienceDTO, Ingredience> ingredienceDTOConverter,
                                 DTOConverter<RatingDTO, Rating> ratingDTOConverter){
        this.beerService = beerService;
        this.ingredienceService = ingredienceService;
        this.ratingService = ratingService;
        this.beerDTOConverter = beerDTOConverter;
        this.ingredienceDTOConverter = ingredienceDTOConverter;
        this.ratingDTOConverter = ratingDTOConverter;
    }
    @GetMapping
    public List<IngredienceDTO> getIngrediences(){
        return ingredienceService.getIngrediences().stream().map(ingredienceDTOConverter::toDTO).toList();

    }
    @GetMapping(path = "/{id}")
    public IngredienceDTO getIngredience(@PathVariable("id") Long id){
        return ingredienceDTOConverter.toDTO(ingredienceService.getIngredienceById(id));
    }
    @GetMapping(path = "/{id}/beers")
    public List<BeerDTO> getBeers(@PathVariable("id") Long id){
        return ingredienceService.getIngredienceById(id).getBeers().stream().map(beerDTOConverter::toDTO).toList();
    }
    @PostMapping
    public IngredienceDTO addIngredience(@RequestBody IngredienceDTO ingredience){
        return ingredienceDTOConverter.toDTO(ingredienceService.addIngredience(ingredienceDTOConverter.toEntity(ingredience)));
    }
    @PutMapping(path = "/{id}")
    public IngredienceDTO updateIngredience(@PathVariable("id") Long id, @RequestBody IngredienceDTO ingredience){
        Ingredience entity = ingredienceDTOConverter.toEntity(ingredience);
        entity.setId(id);
        return ingredienceDTOConverter.toDTO(ingredienceService.updateIngredience(entity));
    }
    @DeleteMapping(path = "/{id}")
    public void deleteIngredience(@PathVariable("id") Long id){
        ingredienceService.deleteIngredience(id);
    }

}
