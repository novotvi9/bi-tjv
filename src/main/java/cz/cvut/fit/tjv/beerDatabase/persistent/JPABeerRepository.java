package cz.cvut.fit.tjv.beerDatabase.persistent;

import cz.cvut.fit.tjv.beerDatabase.domain.Beer;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface JPABeerRepository extends JpaRepository<Beer,Long> {
    public Beer findBeerByName(String name);
    @Query("SELECT b FROM Beer b JOIN b.reviews r WHERE r.rating >= :rating")
    public List<Beer> findBeersByReviewsGreaterThanEqual(@Param("rating") Long rating);
}
