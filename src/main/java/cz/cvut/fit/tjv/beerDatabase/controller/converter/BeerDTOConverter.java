package cz.cvut.fit.tjv.beerDatabase.controller.converter;

import cz.cvut.fit.tjv.beerDatabase.Service.IngredienceServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.Service.RatingServiceInterface;
import cz.cvut.fit.tjv.beerDatabase.controller.dto.BeerDTO;
import cz.cvut.fit.tjv.beerDatabase.domain.Beer;
import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import cz.cvut.fit.tjv.beerDatabase.domain.Rating;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
@Component
public class BeerDTOConverter implements DTOConverter<BeerDTO, Beer> {

    private final IngredienceServiceInterface ingredienceService;
    private final RatingServiceInterface ratingService;
    public BeerDTOConverter(IngredienceServiceInterface ingredienceService, RatingServiceInterface ratingService){
        this.ingredienceService = ingredienceService;
        this.ratingService = ratingService;
    }
    @Override
   public BeerDTO toDTO(Beer beer){
       return new BeerDTO(
               beer.getId(),
               beer.getName(),
               beer.getIngrediences().stream().map(Ingredience::getId).toList(),
               beer.getReviews().stream().map(Rating::getId).toList());
   }
   @Override
   public Beer toEntity(BeerDTO dto){
       return new Beer(
               dto.getId(),
               dto.getName(),
               dto.getIngredienceIds().stream().map(ingredienceService::getIngredienceById).toList(),
               dto.getReviewIds().stream().map(ratingService::getRatingById).toList());
   }



}
