package cz.cvut.fit.tjv.beerDatabase.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import java.util.List;

@Getter
@AllArgsConstructor
public class IngredienceDTO {
    private final Long id;
    private final String name;
    private final List<Long> beerIds;

}
