package cz.cvut.fit.tjv.beerDatabase;

import cz.cvut.fit.tjv.beerDatabase.Service.BeerService;
import cz.cvut.fit.tjv.beerDatabase.Service.IngredienceService;
import cz.cvut.fit.tjv.beerDatabase.domain.Beer;
import cz.cvut.fit.tjv.beerDatabase.domain.Ingredience;
import cz.cvut.fit.tjv.beerDatabase.persistent.JPABeerRepository;
import cz.cvut.fit.tjv.beerDatabase.persistent.JPAIngredienceRepository;
import jakarta.persistence.EntityExistsException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ExtendWith(MockitoExtension.class)
@SpringBootTest
@ActiveProfiles("test")
public class Tests {
    @Autowired
    JPABeerRepository repo;
    @Autowired
    JPAIngredienceRepository repo1;
    @Autowired
    BeerService beerService = new BeerService(repo, repo1);
    @Mock
    private JPAIngredienceRepository ingredienceRepository;

    @InjectMocks
    private IngredienceService ingredienceService;



    @Test
   public void getBeersTest() {

       if(beerService.getBeers().size()==6)
           System.out.println("OK");
       else
           throw new RuntimeException("Not OK");
   }

   @Test
    public void getBeerById(){
       Long beerId = 1L;
         Beer beer = beerService.getBeerById(beerId);
         assertNotNull(beer);
         assertEquals(beerId, beer.getId());
   }

    @Test
    void testGetIngredienceById() {

        Long id = 1L;
        Ingredience mockIngredience = new Ingredience(id, "TestIngredience");
        when(ingredienceRepository.findById(id)).thenReturn(Optional.of(mockIngredience));

        Ingredience result = ingredienceService.getIngredienceById(id);

        assertNotNull(result);
        assertEquals(id, result.getId());
        assertEquals("TestIngredience", result.getName());
    }

    @Test
    void testGetIngrediences() {

        List<Ingredience> mockIngrediences = new ArrayList<>();
        mockIngrediences.add(new Ingredience(1L, "Ingredient1"));
        mockIngrediences.add(new Ingredience(2L, "Ingredient2"));
        when(ingredienceRepository.findAll()).thenReturn(mockIngrediences);

        List<Ingredience> result = ingredienceService.getIngrediences();

        assertNotNull(result);
        assertEquals(2, result.size());
    }

    @Test
    void testAddExistingIngredience() {
        Ingredience ingredienceToAdd = new Ingredience(null, "ExistingIngredient");
        when(ingredienceRepository.findIngrediencesByName("ExistingIngredient")).thenReturn(new Ingredience(1L, "ExistingIngredient"));

        assertThrows(EntityExistsException.class, () -> ingredienceService.addIngredience(ingredienceToAdd));
    }


}